FROM openjdk:8u342-jre

WORKDIR /dcv
COPY target/dcv_*_all.deb .
RUN dpkg -i dcv_*_all.deb
COPY start.sh .

CMD ./start.sh

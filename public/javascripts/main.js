$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
$('.dcv-tooltip').hover(
  function() {
	  $(this).addClass('text-white bg-info');
  },
  function() {
	  $(this).removeClass('text-white bg-info');
  }
);

$(function () {
    $( '#searchable-container' ).searchable({
        searchField: '#container-search',
        selector: '.nav-link',
        childSelector: '#verb-link',
        show: function( elem ) {
            elem.slideDown(100);
        },
        hide: function( elem ) {
            elem.slideUp( 100 );
        }
    })
});
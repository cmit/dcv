# Dicționar de cadre verbale online

## Release
Executa de pe branch-ul `master`:
```
sbt release
```
Apoi urmareste pe pipeline jobul de tag care va crea un fisiser `.deb` si o imagine docker.

## Instalare DCV pe server

**În prealabil** trebuie să fie instalat un client SSH pe mașina locală pentru a accesa serverul.
Cel mai popular pentru Windows e [Putty](https://www.putty.org/), pentru Linux e clientul din linia de comandă `ssh` (openssh).
Configurează în Putty o sesiune de acces la server. Detaliile de acces se trimit pe e-mail.

După toate acestea urmează pașii:

1. Mers în folderul `apps/dcv-stack`
```
cd ~/apps/dcv-stack
```

2. Editat fișierul `.env` și schimbată valoarea variabilei `DCV_VERSION` cu valoarea versiunii noi:
De exemplu înainte de editare
```
DCV_VERSION=v1.0.6
```
, iar după editare:
```
DCV_VERSION=v1.0.7
```
Lista versiunilor disponibile se găsește [aici](https://gitlab.com/cmit/dcv/container_registry/3425113).

3. Oprire serviciu
```
docker compose down
```

4. Pornire serviciu
```
docker compose up -d
```

5. (opțional) Verificare stare serviciu `dcv`:
Serviciului îi poate lua ceva timp până pornește complet, de aceea uneori e util să vedem in ce stare e. Acest lucru se face cu comanda
```
docker ps
```
care ar da un rezultat ce arată cam așa (pot fi mai multe containere de docker pornite):
```
CONTAINER ID   IMAGE                                                 COMMAND                  CREATED        STATUS        PORTS                                                      NAMES
a64984cdf95a   registry.gitlab.com/cmit/dcv:v1.0.6                   "/bin/sh -c ./start.…"   6 days ago     Up 6 days     0.0.0.0:9000->9000/tcp                                     dcv-stack-dcv-1
6f1cd337d318   phpmyadmin/phpmyadmin                                 "/docker-entrypoint.…"   7 days ago     Up 7 days     0.0.0.0:8080->80/tcp                                       phpmyadmin
```
Importantă e linia cu `registry.gitlab.com/cmit/dcv:v1.0.6` și coloana `STATUS` care trebuie să scrie `Up` urmat de perioada ce a trecut de când e în acestă stare.
   
**Notă:** `dcv` pornește automat la fiecare restart al mașinii virtuale. Configurația docker se află în fișierul `docker-compose.yml`.

## Actualizare dictionar

**În prealabil** trebuie să fie instalat un client SSH pe mașina locală pentru a accesa serverul.
Cel mai popular pentru Windows e [Putty](https://www.putty.org/), pentru Linux e clientul din linia de comandă `ssh` (openssh).
Configurează în Putty o sesiune de acces la server. Detaliile de acces se trimit pe e-mail.

După toate acestea:

1. Mers în folderul `apps/dcv-stack`
```
cd ~/apps/dcv-stack
```

2. Copiere fisier nou dictionar (de obicei are numele `dcv-x.y.xml`, `x.y` reprezinta noua versiune, de ex. `0.2`) în subfolderul: `dictionary`. Tot aici se gaseste si varianta curenta a dictionarului.

3. Editat fișierul `.env` și schimbată valoarea variabilei `DCV_DICTIONARY_FILENAME` cu numele noului fișier:
De exemplu înainte de editare
```
DCV_DICTIONARY_FILENAME=dcv-1.16.xml
```
, iar după editare:
```
DCV_DICTIONARY_FILENAME=dcv-1.17.xml
```

3. Oprire serviciu
```
docker compose down
```

4. Pornire serviciu
```
docker compose up -d
```

5. Daca totul e in regula se poate sterge fisierul vechi, in caz contrar se modifica la loc fisieul `.env` si se sterge fisierul nou.

6. (opțional) Verificare stare serviciu `dcv`.
Serviciului îi poate lua ceva timp până pornește complet, de aceea uneori e util să vedem in ce stare e. Acest lucru se face cu comanda
```
docker ps
```
care ar da un rezultat ce arată cam așa (pot fi mai multe containere de docker pornite):
```
CONTAINER ID   IMAGE                                                 COMMAND                  CREATED        STATUS        PORTS                                                      NAMES
a64984cdf95a   registry.gitlab.com/cmit/dcv:v1.0.6                   "/bin/sh -c ./start.…"   6 days ago     Up 6 days     0.0.0.0:9000->9000/tcp                                     dcv-stack-dcv-1
6f1cd337d318   phpmyadmin/phpmyadmin                                 "/docker-entrypoint.…"   7 days ago     Up 7 days     0.0.0.0:8080->80/tcp                                       phpmyadmin
```
Importantă e linia cu `registry.gitlab.com/cmit/dcv:v1.0.6` și coloana `STATUS` în care trebuie să scrie `Up` urmat de perioada ce a trecut de când e în acestă stare.
 

import ReleaseTransformations._

name := """dcv"""
organization := "ro.ilir"

version := (version in ThisBuild).value
releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,              // : ReleaseStep
  inquireVersions,                        // : ReleaseStep
  runClean,                               // : ReleaseStep
  runTest,                                // : ReleaseStep
  setReleaseVersion,                      // : ReleaseStep
  commitReleaseVersion,                   // : ReleaseStep, performs the initial git checks
  pushChanges,                            // : ReleaseStep, also checks that an upstream branch is properly configured
  tagRelease,                             // : ReleaseStep
  pushChanges,                            // : ReleaseStep, also checks that an upstream branch is properly configured
  //publishArtifacts,                       // : ReleaseStep, checks whether `publishTo` is properly set up
  setNextVersion,                         // : ReleaseStep
  commitNextVersion,                      // : ReleaseStep
  pushChanges                             // : ReleaseStep, also checks that an upstream branch is properly configured
)

lazy val root = (project in file(".")).enablePlugins(PlayScala)

Linux / maintainer := "Catalin Mititelu <catalinmititelu@yahoo.com>"

Linux / packageSummary  := "Valence Lexicon of Romanian Verbs (DCV)"

packageDescription := "A detailed description of the complements in the valency frames of the Romanian verbs"

rpmRelease := "1"

rpmVendor := "Institute of Linguistics Iorgu Iordan - Al. Rosetti"

rpmUrl := Some("https://gitlab.com/cmit/dcv")

rpmLicense := Some("Apache v2")

scalaVersion := "2.13.8"

libraryDependencies += guice
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.3.0"
libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.12.0"
libraryDependencies += "com.digitaltangible" %% "play-guard" % "2.5.0"
libraryDependencies += "org.webjars" % "bootstrap" % "4.5.0"

fork := true

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "ro.ilir.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "ro.ilir.binders._"


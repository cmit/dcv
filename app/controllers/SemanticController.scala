package controllers

import javax.inject.Inject
import models.ArgUnit
import models.Dictionary
import models.Verb
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.AbstractController
import play.api.mvc.ControllerComponents
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class SemanticController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  implicit val orderingByPos: Ordering[Tuple2[String, List[Verb]]] = Ordering.by(feat => feat._1)

  import dictionary.romanianOrdering

  def semantics = Action { implicit request =>
    val verbsBySem = dictionary.verbsBySem
    val (sem, verbs) = verbsBySem.toSeq.min
    Ok(views.html.semantics(sem, verbs.headOption, verbsBySem))
  }

  def showSem(sem: String) = Action { implicit request =>
    val verbsBySem = dictionary.verbsBySem
    val verbs = verbsBySem.get(sem)
    Ok(views.html.semantics(sem, verbs.flatMap(_.headOption), verbsBySem))
  }

  def showVerb(sem: String, verbId: String) = Action { implicit request =>
    val verbsBySem = dictionary.verbsBySem
    val verbs = verbsBySem.get(sem)
    Ok(views.html.semantics(sem, verbs.flatMap(_.find(v => v.id.toLowerCase == verbId.toLowerCase)), verbsBySem))
  }

}

object SemanticController {

  def showUnit(sem: String)(argUnit: ArgUnit): Boolean = {
    argUnit.argStructures.exists {
      argStructure =>
        argStructure.argPositions.exists {
          argPosition =>
            argPosition.arguments.exists {
              argument => argument.allSemantics.contains(sem)
            }
        }
    }
  }

}
package controllers

import javax.inject.Inject
import models.Dictionary
import models.Verb
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.AbstractController
import play.api.mvc.ControllerComponents
import models.ArgUnit
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class AlternationController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  implicit val orderingByAlternation: Ordering[Tuple2[Int, List[Verb]]] = Ordering.by(p => p._1)

  import dictionary.romanianOrdering

  def alternations = Action { implicit request =>
    val verbsByAlternation = dictionary.verbsByAlternation
    val (c, verbs) = verbsByAlternation.toSeq.min
    Ok(views.html.alternations(c, verbs.headOption, verbsByAlternation))
  }
  
  def showAlternation(alternationStr: String) = Action { implicit request =>
    val verbsByAlternation = dictionary.verbsByAlternation
    val alternation = alternationStr.toInt
    val verbs = verbsByAlternation.get(alternation)
    Ok(views.html.alternations(alternation, verbs.flatMap(_.headOption), verbsByAlternation))
  }

  def showVerb(alternationStr: String, verbId: String) = Action { implicit request =>
    val verbsByAlternation = dictionary.verbsByAlternation
    val alternation = alternationStr.toInt
    val verbs = verbsByAlternation.get(alternation)
    Ok(views.html.alternations(alternation, verbs.flatMap(_.find(v => v.id.toLowerCase == verbId.toLowerCase)), verbsByAlternation))
  }
  
}

object AlternationController {

  def showUnit(alternation: Int)(argUnit: ArgUnit): Boolean =
    argUnit.argStructures.size == alternation
}

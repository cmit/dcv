package controllers

import javax.inject.Inject
import models.Dictionary
import models.Verb
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.AbstractController
import play.api.mvc.ControllerComponents
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class ComplexityController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  implicit val orderingByComplexity: Ordering[Tuple2[Int, List[Verb]]] = Ordering.by(p => p._1)

  import dictionary.romanianOrdering

  def complexity = Action { implicit request =>
    val verbsByComplexity = dictionary.verbsByComplexity
    val (c, verbs) = verbsByComplexity.toSeq.min
    Ok(views.html.complexity(c, verbs.headOption, verbsByComplexity))
  }
  
  def showComplexity(complexityStr: String) = Action { implicit request =>
    val verbsByComplexity = dictionary.verbsByComplexity
    val complexity = complexityStr.toInt
    val verbs = verbsByComplexity.get(complexity)
    Ok(views.html.complexity(complexity, verbs.flatMap(_.headOption), verbsByComplexity))
  }

  def showVerb(complexityStr: String, verbId: String) = Action { implicit request =>
    val verbsByComplexity = dictionary.verbsByComplexity
    val complexity = complexityStr.toInt
    val verbs = verbsByComplexity.get(complexity)
    Ok(views.html.complexity(complexity, verbs.flatMap(_.find(v => v.id.toLowerCase == verbId.toLowerCase)), verbsByComplexity))
  }
  
}

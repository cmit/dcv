package controllers

import javax.inject.Inject
import models.ArgUnit
import models.Dictionary
import models.Pos
import models.PosClassifiers
import models.Verb
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.AbstractController
import play.api.mvc.ControllerComponents
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class PosController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  implicit val orderingByPos: Ordering[Tuple2[Pos, List[Verb]]] = Ordering.by(pos => pos._1.name)

  import dictionary.romanianOrdering

  def poses = Action { implicit request =>
    val classifier = PosClassifiers.list.head
    val verbsByClassifier = dictionary.verbsByClassifier
    val verbs = verbsByClassifier.get(classifier).getOrElse(Nil)
    Ok(views.html.poses(classifier, verbs.headOption, verbsByClassifier))
  }

  def showPos(classifierName: String) = Action { implicit request =>
    val classifier = PosClassifiers.list
      .find(_.name == classifierName)
      .getOrElse(PosClassifiers.NoClassifier)
    val verbsByClassifier = dictionary.verbsByClassifier
    val verbs = verbsByClassifier.get(classifier).getOrElse(Nil)
    Ok(views.html.poses(classifier, verbs.headOption, verbsByClassifier))
  }

  def showVerb(classifierName: String, verbId: String) = Action { implicit request =>
    val classifier = PosClassifiers.list
      .find(_.name == classifierName)
      .getOrElse(PosClassifiers.NoClassifier)
    val verbsByClassifier = dictionary.verbsByClassifier
    val verbs = verbsByClassifier.get(classifier)
    Ok(views.html.poses(classifier, verbs.flatMap(_.find(v => v.id.toLowerCase == verbId.toLowerCase)), verbsByClassifier))
  }

}

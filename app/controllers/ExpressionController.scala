package controllers

import javax.inject.Inject
import models.Dictionary
import models.Verb
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.AbstractController
import play.api.mvc.ControllerComponents
import models.ArgUnit
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class ExpressionController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  implicit val orderingByExpression: Ordering[Tuple2[Int, List[Verb]]] = Ordering.by(p => p._1)

  import dictionary.romanianOrdering

  def expressions = Action { implicit request =>
    val verbsByExpression = dictionary.verbsByExpression
    Ok(views.html.expressions(verbsByExpression.headOption, verbsByExpression))
  }
  
  def showVerb(verbId: String) = Action { implicit request =>
    val verbsByExpression = dictionary.verbsByExpression
    Ok(views.html.expressions(verbsByExpression.find(v => v.id.toLowerCase == verbId.toLowerCase), verbsByExpression))
  }

}

object ExpressionController {
  def showUnit()(argUnit: ArgUnit): Boolean = false
}

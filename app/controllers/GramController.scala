package controllers

import javax.inject.Inject
import models.ArgUnit
import models.Dictionary
import models.Gram
import models.Gram.Case
import models.Gram.Type
import models.Verb
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.AbstractController
import play.api.mvc.ControllerComponents
import models.Gram.Preposition
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class GramController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  implicit val orderingByGram: Ordering[Tuple2[String, List[Verb]]] = Ordering.by(gram => gram._1)
  implicit val orderingByVerb: Ordering[Verb] = Ordering.by(verb => verb.id)

  import dictionary.romanianOrdering

  def grams = Action { implicit request =>
    val verbsByGram = dictionary.verbsByGram(Case)
    val (gram, verbs) = verbsByGram.toSeq.min
    Ok(views.html.grams(Case, gram, verbs.sorted.headOption, dictionary))
  }

  def showType(`type`: String) = Action { implicit request =>
    val verbsByGram = dictionary.verbsByGram(Gram(`type`))
    val (gram, verbs) = verbsByGram.toSeq.min
    Ok(views.html.grams(Gram(`type`), gram, verbs.sorted.headOption, dictionary))
  }

  def showGram(`type`: String, gram: String) = Action { implicit request =>
    val verbsByGram = dictionary.verbsByGram(Gram(`type`))
    val verbs = verbsByGram.get(gram)
    Ok(views.html.grams(Gram(`type`), gram, verbs.map(_.sorted).flatMap(_.headOption), dictionary))
  }

  def showVerb(`type`: String, gram: String, verbId: String) = Action { implicit request =>
    val verbsByGram = dictionary.verbsByGram(Gram(`type`))
    val verbs = verbsByGram.get(gram)
    Ok(views.html.grams(Gram(`type`), gram, verbs.flatMap(_.find(v => v.id.toLowerCase == verbId.toLowerCase)), dictionary))
  }

}

object GramController {
  import models.Pos._

  def showUnit(gramType: Type, sGram: String)(argUnit: ArgUnit): Boolean = {
    argUnit.argStructures.exists {
      argStructure =>
        argStructure.argPositions.exists {
          argPosition =>
            argPosition.arguments.exists {
              argument => gramType.sGrams(argument).contains(sGram)
            }
        }
    }
  }

}
package controllers

import javax.inject.Inject
import models.Dictionary
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class AlphabetController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  import dictionary.romanianOrdering

  def alphabet = showLetter(dictionary.letters(0).toString())

  def showLetter(letter: String) = Action { implicit request =>
    val usedLetter = letter.charAt(0).toUpper
    val verbsByLetter = dictionary.verbsByLetter
    val verbsForLetter = verbsByLetter.get(usedLetter)
    Ok(views.html.alphabet(usedLetter, verbsForLetter.flatMap(_.headOption), verbsByLetter))
  }

  def showVerb(verbId: String) = Action { implicit request =>
    val usedLetter = verbId.charAt(0).toUpper
    val verbsByLetter = dictionary.verbsByLetter
    val verbsForLetter = verbsByLetter.get(usedLetter)
    Ok(views.html.alphabet(usedLetter, verbsForLetter.flatMap(_.find(v => v.id == verbId)), verbsByLetter))
  }

}
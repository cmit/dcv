package controllers

import models.Dictionary
import play.Environment
import play.api.i18n.I18nSupport
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import java.io.File
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class DictionaryFilesController @Inject() (component: ControllerComponents, dictionary: Dictionary, env: Environment, implicit val ec: ExecutionContext)
  extends AbstractController(component) with I18nSupport {

  def downloadFile(fileName: String): Action[AnyContent] = Action { implicit request =>
    assert(fileName.matches("[-_. A-Za-z0-9]+\\.(pdf|dtd)"))
    val file = env.getFile("conf/files/" + fileName)
    Ok.sendFile(
      content = file,
      inline = false,
      fileName = _ => Some(file.getName))
  }

  def downloadDictionary(): Action[AnyContent] = Action { implicit request =>
    val file = dictionary.file
    Ok.sendFile(
      content = file,
      inline = false,
      fileName = _ => Some(file.getName))
  }

}
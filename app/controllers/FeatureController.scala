package controllers

import javax.inject.Inject
import models.ArgUnit
import models.Dictionary
import models.Verb
import play.api.i18n.I18nSupport
import play.api.i18n.Langs
import play.api.mvc.AbstractController
import play.api.mvc.ControllerComponents
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents

class FeatureController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  implicit val orderingByFeature: Ordering[Tuple2[String, List[Verb]]] = Ordering.by(feat => feat._1)

  import dictionary.romanianOrdering

  def features = Action { implicit request =>
    val verbsByFeature = dictionary.verbsByFeature
    val (feature, verbs) = verbsByFeature.toSeq.min
    Ok(views.html.features(feature, verbs.headOption, verbsByFeature))
  }

  def showFeature(feature: String) = Action { implicit request =>
    val verbsByFeature = dictionary.verbsByFeature
    val verbs = verbsByFeature.get(feature)
    Ok(views.html.features(feature, verbs.flatMap(_.headOption), verbsByFeature))
  }

  def showVerb(feature: String, verbId: String) = Action { implicit request =>
    val verbsByFeature = dictionary.verbsByFeature
    val verbs = verbsByFeature.get(feature)
    Ok(views.html.features(feature, verbs.flatMap(_.find(v => v.id.toLowerCase == verbId.toLowerCase)), verbsByFeature))
  }

}

object FeatureController {
  def showUnit(feature: String)(argUnit: ArgUnit): Boolean = argUnit.features.exists(_ == feature) ||
  argUnit.argStructures.exists {
    argStructure => argStructure.features.exists(_ == feature)
  }
}
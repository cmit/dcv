package controllers

import javax.inject.Inject
import models.Dictionary
import play.api.i18n.I18nSupport
import play.api.i18n.Lang
import play.api.i18n.Langs
import play.api.mvc.MessagesAbstractController
import play.api.mvc.MessagesControllerComponents


class DcvController @Inject() (component: MessagesControllerComponents, dictionary: Dictionary, implicit val langs: Langs)
  extends MessagesAbstractController(component) with I18nSupport {

  // Alphabet – Verb Features – Part-of-Speech – Forms – Semantics – Complexity – Alternations– Expressions
  def index = Action { implicit request =>
    Ok(views.html.index(dictionary))
  }

  def changeLanguage(language: String) = Action { implicit request =>
    val referrer = request.headers.get(REFERER).getOrElse("/")
    Redirect(referrer).withLang(Lang(language))
  }

}
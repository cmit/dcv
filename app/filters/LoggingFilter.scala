package filters

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import akka.stream.Materializer
import javax.inject.Inject
import play.api.mvc._
import play.api.Logger

class LoggingFilter @Inject()(implicit val mat: Materializer, ec: ExecutionContext) extends Filter {
  
  val logger = Logger("application")

  def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    
    val startTime = System.currentTimeMillis

    nextFilter(requestHeader).map { result =>
      val endTime     = System.currentTimeMillis
      val requestTime = endTime - startTime
      
      if (result.header.status >= 400) {
        logger.error(
          s"${requestHeader.method} ${requestHeader.uri} took ${requestTime}ms and returned ${result.header.status}"
        )
      } else {
        logger.debug(
          s"${requestHeader.method} ${requestHeader.uri} took ${requestTime}ms and returned ${result.header.status}"
        )
      }
      
      result
    }
  }
}
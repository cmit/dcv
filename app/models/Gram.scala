package models

object Gram {
  private val CaseKey = "case"
  private val PrepositionKey = "preposition"
  private val ConjunctionKey = "conjunction"
  private val AdverbKey = "adverb"
  private val MoodKey = "mood"
  private val PronounKey = "pronoun"

  // MOODS
  private val SUPIN = "sup."
  private val INFINITIV = "inf."
  private val PARTICIPLE = "part."
  private val GERUND = "ger."
  private val INDICATIV = "ind."

  private val MOODS = Seq(SUPIN, INFINITIV, PARTICIPLE, GERUND, INDICATIV)
  private val PRONOUNS = Seq("se", "își")

  val UNSPEC = "_unspec"

  private def isPronoun(gram: String): Boolean = PRONOUNS.exists(pronoun => gram == pronoun)
  private def isMood(gram: String): Boolean = MOODS.exists(mood => gram.contains(mood))

  private val SUPIN_SUFFIX = " " + SUPIN

  case class Selector(
    val condition:       (Argument, String) => Boolean = (argument: Argument, gram: String) => true,
    val surfaceFunction: (String) => String            = (gram: String) => {if(gram.isEmpty) UNSPEC else gram})

  sealed abstract class Type(val name: String, val key: String,
                             val selectors: Seq[Selector]) {

    def argumentSelect(argument: Argument, gram: String): Boolean = {
      for (selector <- selectors)
        if (selector.condition(argument, gram))
          return true
      return false
    }

    def surfaceFunction(argument: Argument, gram: String): String = {
      for (selector <- selectors)
        if (selector.condition(argument, gram))
          return selector.surfaceFunction(gram)
      return gram
    }

    def sGrams(argument: Argument): Seq[String] = {
      val hasSupin = argument.pos == V && argument.grams.contains(SUPIN_SUFFIX)
      val grams =
        argument.grams.map {
          gram =>
            if (hasSupin && !isMood(gram))
              gram + SUPIN_SUFFIX
            else
              gram
        }
      for (gram <- grams if (argumentSelect(argument, gram))) yield surfaceFunction(argument, gram)
    }

    override def toString = name
  }

  case object Case extends Type(CaseKey, "grams.cases",
    Seq(
      Selector(
        (argument: Argument, gram: String) =>
          argument.pos == GN),
      Selector(
        (argument: Argument, gram: String) =>
          argument.pos == GAdj)))

  case object Preposition extends Type(PrepositionKey, "grams.prepositions",
    Seq(
      Selector((argument: Argument, gram: String) =>
        argument.pos == GP),
      Selector(
        (argument: Argument, gram: String) =>
          argument.pos == V && gram.endsWith(SUPIN_SUFFIX),
        (gram: String) => {
          val index = gram.indexOf(SUPIN_SUFFIX)
          if (index > 0) gram.substring(0, index)
          else gram
        })))

  case object Conjunction extends Type(ConjunctionKey, "grams.conjuctions",
    Seq(Selector((argument: Argument, gram: String) =>
      (argument.pos == GV || argument.pos == V) 
        && !isMood(gram) && !isPronoun(gram)
    )))

  case object Adverb extends Type(AdverbKey, "grams.adverbs", Seq(
    Selector(
      (argument: Argument, gram: String) =>
        argument.pos == GAdv)))

  case object Mood extends Type(MoodKey, "grams.moods", Seq(
    Selector(
      (argument: Argument, gram: String) =>
        (argument.pos == V || argument.pos == GV )&& isMood(gram),
      (gram: String) => {
        MOODS.find(mood => gram.contains(mood)).getOrElse(gram)
      })))

  case object Pronoun extends Type(PronounKey, "grams.pronouns",
    Seq(Selector((argument: Argument, gram: String) =>
      argument.pos == V && isPronoun(gram))))
  

  def apply(name: String): Type = {
    name match {
      case CaseKey        => Case
      case PrepositionKey => Preposition
      case ConjunctionKey => Conjunction
      case AdverbKey      => Adverb
      case MoodKey        => Mood
      case PronounKey        => Pronoun
      case _              => throw new IllegalArgumentException(name)
    }
  }

  def types: Seq[Type] =
    Seq(
      Case,
      Preposition,
      Conjunction,
      Adverb,
      Mood,
      Pronoun
    )
}
package models

import play.api.i18n.Messages
import play.twirl.api.Html
import org.apache.commons.lang3.StringUtils._

object Details {

  def mkDynamicTooltip(prefix: String, value: Any)(implicit messages: Messages): Html = {
    val tooltip = prefix +
      stripAccents(
          removeStart(
            removeEnd(value.toString.replace(' ', '_'), "."),
            "+"
          )
      )
    if (messages.isDefinedAt(tooltip))
      mkTooltip(tooltip, value)
    else
      Html(value.toString)
  }

  def mkTooltip(tooltip: String, value: Any)(implicit messages: Messages): Html =
    Html("<span class=\"dcv-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"" +
      messages(tooltip) + "\">" + value + "</span>")

}
package models

import Pos._

abstract class Pos(val name: String) {
  override def toString = name
}

case object GAdj extends Pos(GAdjString)
case object GAdv extends Pos(GAdvString)
case object GN extends Pos(GNString)
case object GP extends Pos(GPString)
case object GV extends Pos(GVString)
case object V extends Pos(VString)
case object ZeroValent extends Pos(ZeroValentString)
case object NoPos extends Pos("")

object Pos {
  val GAdjString = "GAdj"
  val GAdvString = "GAdv"
  val GNString = "GN"
  val GPString = "GP"
  val GVString = "GV"
  val VString = "V"
  val ZeroValentString = "zerovalent"

  def apply(name: String): Pos = {
    name match {
      case GAdjString       => GAdj
      case GAdvString       => GAdv
      case GNString         => GN
      case GPString         => GP
      case GVString         => GV
      case VString          => V
      case ZeroValentString => ZeroValent
      case _                => throw new IllegalArgumentException(name)
    }
  }
}

case class Classifier(name: String, key: Pos, argumentSelector: (Argument) => Boolean = (x: Argument) => true) {
  def showUnit(argUnit: ArgUnit): Boolean =
    argUnit.argStructures.exists {
      argStructure =>
        argStructure.argPositions.exists {
          argPosition => argPosition.arguments.exists(arg => arg.pos == key && argumentSelector(arg))
        }
    }

  def select(verb: Verb): Boolean =
    verb.argUnits.exists(argUnit => showUnit(argUnit))
}

object PosClassifiers {
  val GAdjClassifier = Classifier("GAdj", GAdj)
  val GAdvClassifier = Classifier("GAdv", GAdv)
  val GNClassifier = Classifier("GN", GN)
  val GPClassifier = Classifier("GP", GP)
  val GVControlClassifier = Classifier("GVControl", GV, (arg: Argument) => arg.semantics.size == 0)
  val GVProperClassifier = Classifier("GVProper", GV, (arg: Argument) => arg.semantics.size > 0)
  val VClassifier = Classifier("V", V)
  val ZerovalentClassifier = Classifier("zerovalent", ZeroValent)
  val NoClassifier = Classifier("", NoPos)

  val list = List(GAdjClassifier, GAdvClassifier, GNClassifier, GPClassifier, GVControlClassifier, GVProperClassifier, VClassifier, ZerovalentClassifier)
}

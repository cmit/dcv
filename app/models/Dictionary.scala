package models

import com.typesafe.config.Config
import play.Environment

import java.io.File
import java.text.Collator
import java.util.Locale
import javax.inject.{Inject, Singleton}
import scala.language.postfixOps
import scala.util.matching.Regex
import scala.xml.{Node, XML}

sealed abstract class FeaturedNode(xml: Node) {
  private def getFeature(attr: String): Seq[String] = {
    for {
      lemmaFeatureNode <- xml \ "LemmaFeature"
      feature <- lemmaFeatureNode.attribute(attr).map(_.mkString(" "))
    } yield feature
  }

  val reflFeatures: Seq[String] = getFeature("ReflForm")
  val negFeatures: Seq[String] = getFeature("NegForm")
  val flexFeatures: Seq[String] = getFeature("FlexForm")
  val features: Seq[String] = reflFeatures ++ negFeatures ++ flexFeatures
}

class Verb(xml: Node) {
  val lemma: String = (xml \@ "LemmaSign").trim
  val homonymNumber: Option[String] = {
    val number = (xml \@ "HomonymNumber").trim
    if (number.isEmpty)
      None
    else 
      Some(number)
  }

  val id: String = lemma + homonymNumber.map(n => "." + n).getOrElse("")
  
  def firstLetter: Char = lemma.charAt(0).toUpper

  def argUnits: Seq[ArgUnit] = (xml \ "ArgUnit").map(u => new ArgUnit(u))

  def expressions: Seq[Expression] = (xml \ "ExpressionList" \ "Expression").map(u => new Expression(u))
}

class ArgUnit(xml: Node) extends FeaturedNode(xml) {
  import ArgUnit.digits

  val romanNumeral: String = toRomanNumerals((xml \@ "ArgUnitNr").toInt, digits)

  private def toRomanNumerals(number: Int, digits: List[(String, Int)]): String = digits match {
    case Nil    => ""
    case h :: t => h._1 * (number / h._2) + toRomanNumerals(number % h._2, t)
  }

  val argStructures: Seq[ArgStructure] = (xml \ "ArgStructure").map(u => new ArgStructure(u))

  val senses: Seq[Sense] = (xml \ "Sense").map(u => new Sense(u))

}
object ArgUnit {
  val digits: List[(String, Int)] = List(("M", 1000), ("CM", 900), ("D", 500), ("CD", 400), ("C", 100), ("XC", 90),
    ("L", 50), ("XL", 40), ("X", 10), ("IX", 9), ("V", 5), ("IV", 4), ("I", 1))
}

class Sense(xml: Node) {
  val number: Option[Char] = {
    val senseNumber: String = (xml \@ "SenseNumber").trim
    if (senseNumber.nonEmpty)
      Some(('a' to 'z')(senseNumber.toInt - 1))
    else
      None
  }

  val definitions: Seq[String] = (xml \ "Definition").map(_ \@ "Definition" trim)

  val examples: Seq[String] = (xml \ "Example").map(_ \@ "Example" trim)
}

class ArgStructure(xml: Node) extends FeaturedNode(xml) {
  val argPositions: Seq[ArgPosition] = (xml \ "ArgPosition").map(u => new ArgPosition(u))
}

class ArgPosition(xml: Node) {
  val number: String = xml \@ "PositionNr" trim
  val arguments: Seq[Argument] = (xml \ "Argument").map(u => new Argument(u))
  val state: String = xml \ "PositionState" \@ "PositionState" trim
}

class Argument(xml: Node) {
  val pos: Pos = {
    val pos = (xml \@ "POS").trim
    if (pos.isEmpty)
      ZeroValent
    else
      Pos(pos)
  }

  val grams: Seq[String] = {
    val gram = xml \ "AFeature" \@ "Gram" trim
    val gramValue = if (gram.startsWith("(") && gram.endsWith(")"))
      gram.substring(1, gram.length() - 1)
    else
      gram
    if (gramValue.isEmpty || gramValue.charAt(gramValue.length - 1) == '\u2026')
      Seq(gramValue)
    else
      Argument.slashSep.split(gramValue).map(_.trim).toSeq
  }
  
  val sem: String = xml \ "AFeature" \@ "Sem" trim
  val semantics: Seq[String] = if (sem.isEmpty) Seq() else Argument.slashSep.split(sem).toIndexedSeq
  val semanticItems: Seq[Seq[String]] = semantics.map(Argument.semItemsSep.split).map(_.toSeq)
  val allSemantics: Seq[String] = semanticItems.flatten ++ semantics
}
object Argument {
  private val slashSep = "\\s*/\\s*".r
  private val semItemsSep = "\\s+".r
}

class Expression(xml: Node) {

  val sign: String = xml \@ "LemmaSign" trim
  val definition: String = xml \@ "Definition" trim

}
object Expression {
  private val ital_regex = "%i(.+?)%i".r
  private val bold_regex = "%b(.+?)%b".r

  def noitalic: String => String = replace(ital_regex, "<span class=\"noitalic\">$1</span>")
  def italic: String => String = replace(ital_regex, "<span class=\"font-italic\">$1</span>")
  def bold: String => String = replace(bold_regex, "<span class=\"font-weight-bold\">$1</span>")

  private def replace(regex: Regex, str: String)(s: String): String = regex.replaceAllIn(s, str)

}

@Singleton
class Dictionary @Inject() (env: Environment, config: Config) {

  // Ordering for Romanian language
  implicit val romanianOrdering: Ordering[String] = Ordering.comparatorToOrdering(
    Collator.getInstance(Locale.forLanguageTag("ro-RO")))
    .asInstanceOf[Ordering[String]]

  lazy val file: File = new File(config.getString("dictionary.file"))
  
  lazy val allVerbs: List[Verb] = {
    val dictionaryXml = XML.loadFile(file)
    val dictionary = dictionaryXml \ "Dictionary" \ "Language"
    val verbXmlList = dictionary \ "Lemma"
    verbXmlList.map(verbNode => new Verb(verbNode)).toList
  }

  lazy val letters: Array[Char] = verbsByLetter.keys.toArray.sortBy(c => c.toString)

  def verbsByLetter: Map[Char, List[Verb]] = allVerbs.groupBy(v => v.firstLetter)

  def verbsByFeature: Map[String, List[Verb]] = {
    val v = for {
      verb <- allVerbs
      argUnit <- verb.argUnits
      feature <- argUnit.features ++ argUnit.argStructures.flatMap(_.features)
    } yield feature -> verb
    v.groupBy(_._1).view.mapValues(values => values.map(v => v._2).distinct).toMap
  }

  def verbsByClassifier: Map[Classifier, List[Verb]] = {
    val v = for {
      verb <- allVerbs
      classifier <- PosClassifiers.list
      if classifier.select(verb)
    } yield classifier -> verb
    v.groupBy(_._1).view.mapValues(values => values.map(v => v._2).distinct).toMap
  }

  def verbsByGram(gramType: Gram.Type): Map[String, List[Verb]] = {
    val v = for {
      verb <- allVerbs
      argUnit <- verb.argUnits
      argStructure <- argUnit.argStructures
      argPosition <- argStructure.argPositions
      argument <- argPosition.arguments
      sGram <- gramType.sGrams(argument)
    } yield sGram -> verb
    v.groupBy(_._1).view.mapValues(values => values.map(v => v._2).distinct).toMap
  }

  def verbsBySem: Map[String, List[Verb]] = {
    val v = for {
      verb <- allVerbs
      argUnit <- verb.argUnits
      argStructure <- argUnit.argStructures
      argPosition <- argStructure.argPositions
      argument <- argPosition.arguments
      semItem <- argument.allSemantics
    } yield semItem -> verb
    v.groupBy(_._1).view.mapValues(values => values.map(v => v._2).distinct).toMap
  }

  def verbsByComplexity: Map[Int, List[Verb]] = {
    val v = for {
      verb <- allVerbs
    } yield verb.argUnits.size -> verb
    v.groupBy(_._1).view.mapValues(values => values.map(v => v._2).distinct).toMap
  }

  def verbsByAlternation: Map[Int, List[Verb]] = {
    val v = for {
      verb <- allVerbs
      argUnit <- verb.argUnits if argUnit.argStructures.size > 1
    } yield argUnit.argStructures.size -> verb
    v.groupBy(_._1).view.mapValues(values => values.map(v => v._2).distinct)toMap
  }

  def verbsByExpression: List[Verb] = allVerbs.filter(_.expressions.nonEmpty)

}
